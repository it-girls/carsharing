# Deployment of _Carshring_ by _IT-Girls_
## For Production
Working installations of Docker and Docker-Compose are required.

The project can be saved to any path. This is referenced as a _`/yourpath/`_ below.

### Installation
1. Clone the projects git repository:
	```shell
	$ cd /yourpath/
	$ git clone https://gitlab.com/it-girls/carsharing
	$ cd ./carsharing/
	$ git submodule init
	$ git submodule update
	```
2. Update the host adress in _`.\carsharing-frontend\nginx.conf`_
	```conf
	server {

		listen 80;
		server_name  it-gurls.de  www.it-gurls.de;

		root   /usr/share/nginx/html;
		index  index.html index.htm;
		include /etc/nginx/mime.types;

		gzip on;
		gzip_min_length 1000;
		gzip_proxied expired no-cache no-store private auth;
		gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;

		location / {
			try_files $uri $uri/ /index.html;
		}

		location /api {
			include uwsgi_params;
			uwsgi_pass flask:8080;
		}
	}
	```
	Change server_name to the desired host address:
	```conf
		server_name  your-host.de;
	```
	or
	```conf
		server_name  1.2.3.4;
	```
3. Build and start the services:
	```shell
	$ docker-compose build
	$ docker-compose up
	```
	To run the services in background use:
	```shell
	$ docker-compose up -d
	```
	To stop all running services use:
	```shell
	$ docker-compose stop
	```
___

## History
- 2020-06-07 Creation
- 2020-07-01 Update

## Author
- Markus Mogck: <m.mogck@live.de>
- Frederick Hastedt: <ben@hastedts.de>